/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-use-before-define */
/* eslint-disable global-require */
const path = require('path');
const webpack = require('webpack');

module.exports = {
    chainWebpack(config) {
        initPathAliases(config);
        useDefineExtra(config);
    },
    pages: {
        index: {
            entry: 'src/main/js/index.js',
            template: 'src/main/template/index.pug',
            filename: 'index.html',
            chunks: ['chunk-vendors', 'chunk-common', 'index'],
        },
    },
};

function initPathAliases(config) {
    config.resolve.alias
        .set('@', path.resolve('./src'))
        .set('@api', path.resolve('./src/api'))
        .set('@components', path.resolve('./src/components'))
        .set('@constants', path.resolve('./src/constants'))
        .set('@enums', path.resolve('./src/enums'))
        .set('@hooks', path.resolve('./src/hooks'))
        .set('@layouts', path.resolve('./src/layouts'))
        .set('@models', path.resolve('./src/models'))
        .set('@pages', path.resolve('./src/pages'))
        .set('@plugins', path.resolve('./src/plugins'))
        .set('@router', path.resolve('./src/router'))
        .set('@store', path.resolve('./src/store'))
        .set('@utils', path.resolve('./src/utils'));
}

function useDefineExtra(config) {
    const toEnvObject = (entries) => entries.reduce(
        (acc, [key, value = (process.env[key] ?? '')]) => ({
            ...acc,
            [`process.env.${key}`]: (
                typeof value === 'boolean'
                    ? `${value}`
                    : `"${value}"`
            ),
        }),
        {},
    );

    config.plugin('define-extra').use(
        webpack.DefinePlugin,
        [
            toEnvObject([
                ['API_URL'],
            ]),
        ],
    );
}
