# aff-test

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### TODO list
    1. Make responsive UI.
    2. Handle bad api responses, make notifications, handle errors.
    3. Make blocker/loader on 'create/edit/delete' resource.
    4. Add confirmations on delete.
    5. Improve UI for resource items.
            - Add links to relative resource (userId, albumId, postId, ...)
            - Alow to use only existing relative resource on edit/create, use select for it.
            - Boolean item data -> checkbox/toggler.
            - Add Fields validation
            - Object fields validation (address, company)
