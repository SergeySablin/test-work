import {
    Album,
    Comment,
    Photo,
    Post,
    Todo,
    User,
} from '@models';
import { resourceCategoryEnum } from '@enums';

export const resourceCategoryToModelMap = {
    [resourceCategoryEnum.ALBUMS]: Album,
    [resourceCategoryEnum.COMMENTS]: Comment,
    [resourceCategoryEnum.PHOTOS]: Photo,
    [resourceCategoryEnum.POSTS]: Post,
    [resourceCategoryEnum.TODOS]: Todo,
    [resourceCategoryEnum.USERS]: User,
};

export const normalizeResourceItem = (resourceCategory, item) => {
    const formattedItem = Object.entries(item).reduce(
        (acc, [key, value]) => {
            if (value instanceof Object) {
                acc[key] = JSON.stringify(value, undefined, 4);
            } else if (typeof value === 'boolean') {
                acc[key] = `${value}`;
            } else {
                acc[key] = value;
            }
            return acc;
        },
        {},
    );
    return new resourceCategoryToModelMap[resourceCategory](formattedItem);
};

export const normalizeResourceData = (resourceCategory, arr) => arr.reduce(
    (acc, item) => {
        acc[item.id] = normalizeResourceItem(resourceCategory, item);
        return acc;
    },
    {},
);

export const getEmptyResourceItem = (resourceCategory) => new resourceCategoryToModelMap[resourceCategory]();

export const getResourceItemClone = (item) => Object.assign(Object.create(Object.getPrototypeOf(item)), item);
