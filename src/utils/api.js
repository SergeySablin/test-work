export const isResponseStatusSuccessful = (status) => status >= 200 && status < 300;
