import { createApp } from 'vue';
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/el-reset.css';
import 'element-plus/lib/theme-chalk/index.css';

import router from '@router';
import store from '@store';
import App from '@/main/app/App.vue';

createApp(App)
    .use(ElementPlus)
    .use(router)
    .use(store)
    .mount('#app');
