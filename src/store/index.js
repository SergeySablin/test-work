import { createStore } from 'vuex';
import resourcesStore from './resources';

export default createStore({
    modules: {
        resources: resourcesStore,
    },
});
