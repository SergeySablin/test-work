import Api from '@api';
import { normalizeResourceData } from '@utils/resource';
import { isResponseStatusSuccessful } from '@utils/api';

export const loadResourceData = async ({ commit }, resourceCategory) => {
    const { status, data } = await Api.get(`/${resourceCategory}`);

    if (!isResponseStatusSuccessful(status)) {
        return;
    }
    commit(
        'setResourceData',
        {
            resourceCategory,
            data: normalizeResourceData(resourceCategory, data),
        },
    );
};

export const updateResource = async ({ commit }, { resourceCategory, id, data }) => {
    const { status, data: responseData } = await Api.patch(`/${resourceCategory}/${id}`, data);

    if (!isResponseStatusSuccessful(status)) {
        return;
    }

    commit(
        'updateResourceItem',
        {
            resourceCategory,
            id,
            data: responseData,
        },
    );
};

export const deleteResource = async ({ commit }, { resourceCategory, id }) => {
    const { status } = await Api.delete(`/${resourceCategory}/${id}`);

    if (!isResponseStatusSuccessful(status)) {
        return;
    }

    commit(
        'deleteResourceItem',
        {
            resourceCategory,
            id,
        },
    );
};

export const createResource = async ({ commit }, { resourceCategory, data }) => {
    const { status, data: responseData } = await Api.post(`/${resourceCategory}`, data);

    if (!isResponseStatusSuccessful(status)) {
        return;
    }

    commit(
        'updateResourceItem',
        {
            resourceCategory,
            data: responseData,
        },
    );
};
