import { normalizeResourceItem } from '@utils/resource';

export const setResourceData = async (state, { resourceCategory, data }) => {
    state[resourceCategory] = data;
};

export const updateResourceItem = async (state, { resourceCategory, id, data }) => {
    state[resourceCategory][id] = normalizeResourceItem(resourceCategory, {
        ...state[resourceCategory][id],
        ...data,
    });
};

export const deleteResourceItem = async (state, { resourceCategory, id }) => {
    delete state[resourceCategory][id];
};
