export const resourceCategoryEnum = Object.freeze({
    POSTS: 'posts',
    COMMENTS: 'comments',
    ALBUMS: 'albums',
    PHOTOS: 'photos',
    TODOS: 'todos',
    USERS: 'users',
});
