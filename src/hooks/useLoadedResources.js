import { computed, watch, ref } from 'vue';
import { useStore } from 'vuex';

export default (resourceCategoriesArr) => {
    const store = useStore();
    const isDataLoaded = ref(false);

    async function loadNeededResources(resourceCategoriesToLoadArr) {
        isDataLoaded.value = false;

        const resourceCategoryToLoadArr = resourceCategoriesToLoadArr.filter(
            (resourceCategory) => !store.getters['resources/getResourceData'](resourceCategory),
        );

        await Promise.all(
            resourceCategoryToLoadArr.map(
                (neededResourceName) => store.dispatch('resources/loadResourceData', neededResourceName),
            ),
        );

        isDataLoaded.value = true;
    }

    watch(
        resourceCategoriesArr,
        loadNeededResources,
        { immediate: true },
    );

    return {
        resources: computed(() => Object.fromEntries(
            resourceCategoriesArr.value.map(
                (resourceCategory) => [resourceCategory, store.getters['resources/getResourceData'](resourceCategory)],
            ),
        )),
        isDataLoaded,
    };
};
