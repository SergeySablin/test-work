import { Resource } from '@/models/resource/Resource';

export class Todo extends Resource {
    constructor({
        id,
        userId,
        title,
        completed,
    } = {}) {
        super({
            id,
        });

        this.userId = userId;
        this.title = title;
        this.completed = completed;
    }
}
