import { Resource } from '@/models/resource/Resource';

export class User extends Resource {
    constructor({
        id,
        name,
        username,
        email,
        address,
        phone,
        website,
        company,
    } = {}) {
        super({
            id,
        });

        this.name = name;
        this.username = username;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.website = website;
        this.company = company;
    }
}
