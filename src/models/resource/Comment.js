import { Resource } from '@/models/resource/Resource';

export class Comment extends Resource {
    constructor({
        id,
        postId,
        name,
        email,
        body,
    } = {}) {
        super({
            id,
        });

        this.postId = postId;
        this.name = name;
        this.email = email;
        this.body = body;
    }
}
