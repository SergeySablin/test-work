import { Resource } from '@/models/resource/Resource';

export class Post extends Resource {
    constructor({
        id,
        userId,
        title,
        body,
    } = {}) {
        super({
            id,
        });

        this.userId = userId;
        this.title = title;
        this.body = body;
    }
}

window.Post = Post;
