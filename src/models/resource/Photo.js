import { Resource } from '@/models/resource/Resource';

export class Photo extends Resource {
    constructor({
        id,
        albumId,
        title,
        url,
        thumbnailUrl,
    } = {}) {
        super({
            id,
        });

        this.albumId = albumId;
        this.title = title;
        this.url = url;
        this.thumbnailUrl = thumbnailUrl;
    }
}
