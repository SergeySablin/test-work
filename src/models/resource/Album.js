import { Resource } from '@/models/resource/Resource';

export class Album extends Resource {
    constructor({
        id,
        userId,
        title,
    } = {}) {
        super({
            id,
        });

        this.userId = userId;
        this.title = title;
    }
}
