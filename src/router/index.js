import { defineAsyncComponent } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { routeMetaEnum, routeNameEnum, routeParamEnum } from '@enums';

const MainLayout = defineAsyncComponent(() => import('@layouts/MainLayout.vue'));

const routes = [
    {
        name: routeNameEnum.RESOURCE_LIST,
        path: `/:${routeParamEnum.RESOURCE_CATEGORY}`,
        components: {
            content: () => import('@pages/ResourcePage.vue'),
        },
        meta: {
            [routeMetaEnum.LAYOUT]: MainLayout,
        },
    },

    {
        path: '/:catchAll(.*)',
        redirect: {
            name: routeNameEnum.RESOURCE_LIST,
            params: {
                [routeParamEnum.RESOURCE_CATEGORY]: 'posts',
            },
        },
    },
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;
